import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SaleService } from './services/sale.service';
import { DatabaseService } from './services/database.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
    
    public selectedIndex = 0;
    
    public appPages = [
        {
            title: 'Productos',
            url: '/folder/Inbox',
            icon: 'mail'
        },
        {
            title: 'Clientes',
            url: '/clients',
            icon: 'paper-plane'
        },
        {
            title: 'Ventas',
            url: '/folder/Favorites',
            icon: 'heart'
        },
        {
            title: 'Creditos',
            url: '/folder/Archived',
            icon: 'archive'
        },
        {
            title: 'Sincronizar',
            url: '/folder/Trash',
            icon: 'trash'
        },
    ];
    
    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private db:DatabaseService
    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            this.db.openDatabase();
        });
    }

    ngOnInit() {
        const path = window.location.pathname.split('folder/')[1];
        if (path !== undefined) {
            this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
        }
    }
}
