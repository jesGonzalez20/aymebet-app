export interface IProduct{
    id?:number;
    nombre:string;
    p1:number;
    p2:number;
    p3:number;
    marca:string;
    existencia:number
}