import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SaleShowPage } from './sale-show.page';

describe('SaleShowPage', () => {
  let component: SaleShowPage;
  let fixture: ComponentFixture<SaleShowPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaleShowPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SaleShowPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
