import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ShoppingbasketService } from 'src/app/services/offline/shoppingbasket.service';
import { SaleService } from 'src/app/services/offline/sale.service';
import { CustomerService } from 'src/app/services/offline/customer.service';
import { Sale } from 'src/app/services/class/Sale';
import { Customer } from 'src/app/services/class/Customer';

@Component({
    selector: 'app-sale-show',
    templateUrl: './sale-show.page.html',
    styleUrls: ['./sale-show.page.scss'],
})
export class SaleShowPage implements OnInit {

    idParams: string;
    public products: any = [];
    public sale: Sale = { total: 0.0, status: 0, date: '', type_sale: 0, customer_id: 0 };
    public customer: Customer = { id: 0, name: '', lastname: '', customer_id_serve: 0, email: '', phone: '', address: '' };

    constructor(
        private router: ActivatedRoute,
        private shoppingBS: ShoppingbasketService,
        private saleS: SaleService,
        private customerS: CustomerService
    ) { }

    ngOnInit() {
        this.idParams = this.router.snapshot.params.id;
        this.loadSale();
        this.loadProducts();
    }
    async loadProducts() {
        try {
            let saleID = parseInt(this.idParams);
            this.products = await this.shoppingBS.getProducts(saleID);
        } catch (err) {
            alert("Ha ocurrido un error al obtener los productos de la vent : " + err.message);
        }
    }
    async loadSale() {
        try {
            let saleID = parseInt(this.idParams);
            this.sale = await this.saleS.getById(saleID);
            this.loadCustomer(this.sale.customer_id);
        } catch (err) {
            alert("Ha ocurrido un error al obtener los productos de la venta : " + err.message);
        }
    }
    async loadCustomer(customerId: number) {
        try {
            this.customer = await this.customerS.getById(customerId);
        } catch (err) {
            alert("Ha ocurrido un error al obtener el cliente de la vent : " + err.message);
        }
    }
    async cancelSale() {
        try {
            let saleID = parseInt(this.idParams);
            await this.saleS.cancelSale(saleID);
            alert("La venta fue cancelada correctamente");
            this.loadSale();
        } catch (err) {
            alert("Ha ocurrido un error al cancelar la venta: "+err.message);
        }
    }
    formaterAmount(amount) {
        const options2 = { style: 'currency', currency: 'USD' };
        const numberFormat2 = new Intl.NumberFormat('en-US', options2);
        return numberFormat2.format(amount);
    }

}
