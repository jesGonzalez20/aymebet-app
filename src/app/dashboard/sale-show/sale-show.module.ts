import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SaleShowPageRoutingModule } from './sale-show-routing.module';

import { SaleShowPage } from './sale-show.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SaleShowPageRoutingModule
  ],
  declarations: [SaleShowPage]
})
export class SaleShowPageModule {}
