import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { LoadingController } from '@ionic/angular';

//importar faker's products
import { productsFake } from '../../fake/products';
import { ProductService } from 'src/app/services/offline/product.service';
import { Product } from 'src/app/services/class/Product';

import { ToastController } from '@ionic/angular';

@Component({
    selector: 'app-products',
    templateUrl: './products.page.html',
    styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {

    public disabled: boolean = false;
    public searchForm: FormGroup;

    //ACTUALIZAR DE TIPO ANY A TIPO IPRODUCT'S INTERFACE
    public productsFake: any = [];
    public products: Product[] = [];
    public skeleton: boolean = true;

    constructor(
        private loadingController: LoadingController,
        private formBuilder: FormBuilder,
        private productS: ProductService,
        private toast: ToastController,
    ) { }

    ngOnInit() {
        //CONSTRUIR EL FORMULARIO 
        this.searchForm = this.formBuilder.group({
            like: new FormControl('', [Validators.required]),
        });
        this.loadProducts();
    }
    async loadProducts() {
        try {
            this.products = await this.productS.getAll();
        } catch (err) {
            alert("Ha ocurrido un error offline " + err.message);
        } finally {
            this.skeleton = false;
        }
    }
    //evento key.enter , se ejecuta cuando el usuario da un enter
    async onSearch() {
        this.skeleton = true;
        const loading = await this.loadingController.create({
            message: 'Espera por favor...',
            duration: 500
        });
        try {
            await loading.present();
            const like = this.searchForm.get("like").value;
            this.products = await this.productS.filter(like);
        } catch (err) {
            alert("Ha ocurrido un error offline " + err.message);
        } finally {
            this.skeleton = false;
            await loading.onDidDismiss();
        }
    }
    async addProducts() {
        let products: any = [];
        try {
            products = await this.productS.getProductsServer('1256766');
        } catch (error) {
            alert("ha ocurrido al agregar productos: " + error.message);
        } finally {
            for (var i = 0; i < products.length; i++) {
                
                let productS = products[i];
                let product = new Product();
                product.product_id_serve = productS.id;
                product.name_product = productS.producto;
                product.brand = productS.marca;
                product.price1 = productS.p1;
                product.price2 = productS.p2;
                product.price3 = productS.p3;
                product.stock = productS.stock;

                try {
                    await this.productS.save(product);
                    const toast = await this.toast.create({
                        header: 'Success',
                        message: 'Producto almacenado correctamente.',
                        color: 'success',
                        position: 'bottom',
                        duration: 3000
                    });
                    toast.present();
                    this.loadProducts();
                } catch (err) {
                    const toast = await this.toast.create({
                        header: 'Erro',
                        message: 'Ha ocurrido un error al guardar un producto: ' + err.message,
                        color: 'danger',
                        position: 'bottom',
                        duration: 3000
                    });
                    toast.present();
                }

            }
        }
    }
}
