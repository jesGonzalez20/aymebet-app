import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SaleCurrentPage } from './sale-current.page';

const routes: Routes = [
  {
    path: '',
    component: SaleCurrentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SaleCurrentPageRoutingModule {}
