import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SaleCurrentPageRoutingModule } from './sale-current-routing.module';

import { SaleCurrentPage } from './sale-current.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SaleCurrentPageRoutingModule
  ],
  declarations: [SaleCurrentPage]
})
export class SaleCurrentPageModule {}
