import { Component, OnInit } from '@angular/core';
import { customerFaker } from 'src/app/fake/customer';
import { SaleService } from 'src/app/services/offline/sale.service';
import { Sale } from 'src/app/services/class/Sale';
import { ShoppingbasketService } from 'src/app/services/offline/shoppingbasket.service';
import { Customer } from 'src/app/services/class/Customer';
import { CustomerService } from 'src/app/services/offline/customer.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/services/offline/product.service';


@Component({
    selector: 'app-sale-current',
    templateUrl: './sale-current.page.html',
    styleUrls: ['./sale-current.page.scss'],
})
export class SaleCurrentPage implements OnInit {

    public total: number;
    public products: any = [];
    public customers: Customer[] = [];

    registerForm: FormGroup;


    constructor(
        private saleS: SaleService,
        private shoppingBS: ShoppingbasketService,
        private customerS: CustomerService,
        private formBuilder: FormBuilder,
        private router: Router,
        private productS: ProductService
    ) { }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            customer_id: new FormControl('', Validators.required),
            type_sale: new FormControl('', Validators.required),
        });
        this.getSaleWithId();
        this.getProducts();
        this.loadCustomers();
    }
    async getSaleWithId() {
        try {
            let saleID = parseInt(localStorage.getItem('saleID'));
            this.total = await this.shoppingBS.getTotalSale(saleID);
        } catch (err) {
            alert("Ha ocurrido un error al obtener la venta actual: " + err.message);
        }
    }
    async getProducts() {

        try {
            let saleID = parseInt(localStorage.getItem('saleID'));
            this.products = await this.shoppingBS.getProducts(saleID);
        } catch (err) {
            alert("Ha ocurrido un error al obtener los productos de la vent : " + err.message);
        }
    }
    async loadCustomers() {
        try {
            this.customers = await this.customerS.getAll();
        } catch (err) {
            alert("Ha ocurrido un error al obtener los clientes de la venta : " + err.message);
        }
    }
    async endSale() {
        let customer = parseInt(this.registerForm.get("customer_id").value);
        let type_sale = parseInt(this.registerForm.get("type_sale").value);
        let saleID = parseInt(localStorage.getItem('saleID'));

        let sale = new Sale();
        sale.id = saleID;
        sale.customer_id = customer;
        sale.type_sale = type_sale;
        sale.total = this.total;
        if (sale.type_sale == 0)
            sale.status = 1;
        else
            sale.status = 3;
        try {
            await this.saleS.endSale(sale);
        } catch (err) {
            alert("Ha ocurrido un error al terminar la venta: " + err.message);
        } finally {
            let saleID = localStorage.getItem('saleID');
            this.discountProducts(saleID);
        }

    }
    async discountProducts(saleID: string) {
        try {
            let saleID = parseInt(localStorage.getItem('saleID'));
            let productsOnShopping = await this.shoppingBS.getProducts(saleID);

            for (let i = 0; i < productsOnShopping.length; i++) {
                let productOnSh = productsOnShopping[i];
                let product = await this.productS.getById(productOnSh.product_id);
                product.stock = product.stock - productOnSh.amount;
                await this.productS.update(product.id,product.stock);
            }
        } catch (error) {
            alert("Error al procesar el stock del producto: "+error.message);
        }
        
        try {
            let result = await this.saleS.initial();
            localStorage.setItem('saleID', result.insertId);
        } catch (err) {
            alert("Ha ocurrido un error al crear la venta" + err.message);
        } finally {
            this.router.navigate(['/dashboard/sales']);
        }
    }
    formaterAmount(amount) {
        const options2 = { style: 'currency', currency: 'USD' };
        const numberFormat2 = new Intl.NumberFormat('en-US', options2);
        return numberFormat2.format(amount);
    }

}
