import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SaleCurrentPage } from './sale-current.page';

describe('SaleCurrentPage', () => {
  let component: SaleCurrentPage;
  let fixture: ComponentFixture<SaleCurrentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaleCurrentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SaleCurrentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
