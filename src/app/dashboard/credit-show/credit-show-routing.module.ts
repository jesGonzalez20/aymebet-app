import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreditShowPage } from './credit-show.page';

const routes: Routes = [
  {
    path: '',
    component: CreditShowPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreditShowPageRoutingModule {}
