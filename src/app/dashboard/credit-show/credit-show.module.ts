import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreditShowPageRoutingModule } from './credit-show-routing.module';

import { CreditShowPage } from './credit-show.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreditShowPageRoutingModule
  ],
  declarations: [CreditShowPage]
})
export class CreditShowPageModule {}
