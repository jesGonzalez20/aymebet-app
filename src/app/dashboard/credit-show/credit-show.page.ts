import { Component, OnInit } from '@angular/core';
import { Customer } from 'src/app/services/class/Customer';
import { Sale } from 'src/app/services/class/Sale';
import { ActivatedRoute } from '@angular/router';
import { SaleService } from 'src/app/services/offline/sale.service';
import { CustomerService } from 'src/app/services/offline/customer.service';
import { Pay } from 'src/app/services/class/Pay';
import { PayService } from 'src/app/services/offline/pay.service';

@Component({
    selector: 'app-credit-show',
    templateUrl: './credit-show.page.html',
    styleUrls: ['./credit-show.page.scss'],
})
export class CreditShowPage implements OnInit {

    idParams: string; 
    public credit: Sale = { total: 0.0, status: 0, date: '', type_sale: 0, customer_id: 0 };
    public customer: Customer = { id: 0, name: '', lastname: '', customer_id_serve: 0, email: '', phone: '', address: '' };
    public pays:Pay[] = [];

    public pay:number = 0.0;

    public totalPay:number = 0.0;

    constructor(
        private router: ActivatedRoute,
        private saleS: SaleService,
        private customerS: CustomerService,
        private payS:PayService,
    ) { }

    ngOnInit() {
        this.idParams = this.router.snapshot.params.id;
        this.loadCredit();
        this.loadPays();
        this.totalPays();
    }
    async totalPays() {
        try {
            let saleID = parseInt(this.idParams);
            this.totalPay = await this.payS.getTotalPays(saleID);
        } catch (err) {
            alert("Ha ocurrido un error el total pagado del credito : " + err.message);
        }
    }
    async loadCredit() {
        try {
            let saleID = parseInt(this.idParams);
            this.credit = await this.saleS.getById(saleID);
            this.loadCustomer(this.credit.customer_id);
        } catch (err) {
            alert("Ha ocurrido un error al obtener el credito : " + err.message);
        }
    }
    async loadCustomer(customerId: number) {
        try {
            this.customer = await this.customerS.getById(customerId);
        } catch (err) {
            alert("Ha ocurrido un error al obtener el cliente de la vent : " + err.message);
        }
    }
    async loadPays(){
        try {
            let saleID = parseInt(this.idParams);
            this.pays = await this.payS.getPays(saleID);
        } catch (err) {
            alert("Ha ocurrido un error al obtener el cliente de la vent : " + err.message);
        }
    }
    async addPay(){
        try {
            if(this.pay != 0 && this.pay != null){
                let saleID = parseInt(this.idParams);
                await this.payS.addPay(this.pay,saleID);
                this.totalPay = await this.payS.getTotalPays(saleID);

                alert("Total pagado: "+this.totalPay);
                alert("Credito TOtal: "+this.credit.total);

                if(this.totalPay == this.credit.total){
                    this.creditPayed();
                }else{
                    alert("El cliente "+this.customer.name+" Aun debe "+this.formaterAmount(this.credit.total - this.totalPay)+ " pesos");
                }

            }else{
                alert("El pago es requerido");
            }
        } catch (err) {
            alert("Ha ocurrido un error al registrar el pago: " + err.message);
        }finally{
            this.loadCredit();
            this.loadPays();
            this.totalPays();
        }
    }
    async creditPayed() {
        try {
            let saleID = parseInt(this.idParams);
            await this.saleS.creditPayed(saleID);
        } catch (err) {
            alert("Ha ocurrido un erro al terminar el credito: "+err.message);
        }finally{
            this.loadCredit();
        }
    }
    formaterAmount(amount) {
        const options2 = { style: 'currency', currency: 'USD' };
        const numberFormat2 = new Intl.NumberFormat('en-US', options2);
        return numberFormat2.format(amount);
    }

}
