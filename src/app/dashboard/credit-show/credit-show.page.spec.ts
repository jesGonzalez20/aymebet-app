import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreditShowPage } from './credit-show.page';

describe('CreditShowPage', () => {
  let component: CreditShowPage;
  let fixture: ComponentFixture<CreditShowPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditShowPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreditShowPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
