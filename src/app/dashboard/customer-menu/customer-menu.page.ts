import { Component, OnInit } from '@angular/core';
import { Customer } from 'src/app/services/class/Customer';
import { ActivatedRoute } from '@angular/router';
import { CustomerService } from 'src/app/services/offline/customer.service';

@Component({
    selector: 'app-customer-menu',
    templateUrl: './customer-menu.page.html',
    styleUrls: ['./customer-menu.page.scss'],
})
export class CustomerMenuPage implements OnInit {

    customer: Customer = { id: 0, customer_id_serve: 0, name: '', lastname: '', email: '', phone: '', address: '' };
    idParams: number;

    items: any = [
        { route: '/dashboard/customer-sales/' + 1, image: 'https://image.flaticon.com/icons/png/512/86/86042.png', title: 'Ventas' },
        { route: '/dashboard/customer-credits/' + 1, image: 'https://image.flaticon.com/icons/png/512/60/60378.png', title: 'Creditos' },
    ];

    constructor(
        private params: ActivatedRoute,
        private customerS: CustomerService
    ) { }

    ngOnInit() {
        this.idParams = parseInt(this.params.snapshot.params.id);
        this.loadCustomer();
    }

    async loadCustomer() {
        let idCustomer = this.idParams;
        try {
            this.customer = await this.customerS.getById(idCustomer);
        } catch (err) {
            alert("Ha ocurrido un erro al traer el cliente: " + err.message);
        }

    }

}
