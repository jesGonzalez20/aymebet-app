import { Component, OnInit } from '@angular/core';
import { Customer } from 'src/app/services/class/Customer';
import { ActivatedRoute } from '@angular/router';
import { CustomerService } from 'src/app/services/offline/customer.service';
import { SaleService } from 'src/app/services/offline/sale.service';

@Component({
    selector: 'app-customer-sales',
    templateUrl: './customer-sales.page.html',
    styleUrls: ['./customer-sales.page.scss'],
})
export class CustomerSalesPage implements OnInit {

    customer: Customer = { id: 0, customer_id_serve: 0, name: '', lastname: '', email: '', phone: '', address: '' };
    idParams: number;
    sales: any[] = [];
    total:number = 0.0;

    constructor(
        private params: ActivatedRoute,
        private customerS: CustomerService,
        private saleS: SaleService
    ) { }

    ngOnInit() {
        this.idParams = parseInt(this.params.snapshot.params.id);
        this.loadCustomer();
        this.loadSales();
        this.loadTotalSale();
    }
    async loadTotalSale() {
        try {
            let idCustomer = this.idParams;
            this.total = await this.saleS.getTotalSaleByCustomer(idCustomer);
        } catch (error) {
            alert("Ha ocurrido un error al obtener las ventas: "+error.message);
      
        }
    }
    async loadSales() {
        try {
            let idCustomer = this.idParams;
            this.sales = await this.saleS.getSalesByIdCustomer(idCustomer);
        } catch (error) {
            alert("Ha ocurrido un error al obtener las ventas: " + error.message);
        }
    }

    async loadCustomer() {
        let idCustomer = this.idParams;
        try {
            this.customer = await this.customerS.getById(idCustomer);
        } catch (err) {
            alert("Ha ocurrido un erro al traer el cliente: " + err.message);
        }
    }
    formaterAmount(amount) {
        const options2 = { style: 'currency', currency: 'USD' };
        const numberFormat2 = new Intl.NumberFormat('en-US', options2);
        return numberFormat2.format(amount);
    }

}
