import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CustomerSalesPageRoutingModule } from './customer-sales-routing.module';

import { CustomerSalesPage } from './customer-sales.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomerSalesPageRoutingModule
  ],
  declarations: [CustomerSalesPage]
})
export class CustomerSalesPageModule {}
