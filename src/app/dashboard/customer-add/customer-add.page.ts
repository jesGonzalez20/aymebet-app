import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Customer } from 'src/app/services/class/Customer';
import { CustomerService } from 'src/app/services/offline/customer.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
    selector: 'app-customer-add',
    templateUrl: './customer-add.page.html',
    styleUrls: ['./customer-add.page.scss'],
})
export class CustomerAddPage implements OnInit {
    
    registerForm: FormGroup;
    submitted = false;
    
    constructor(
        private formBuilder: FormBuilder, 
        private customerS:CustomerService,
        private toast:ToastController,
        private route: Router
    ) { }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            name: new FormControl('', Validators.required),
            lastName: new FormControl('', Validators.required),
            email: new FormControl('', [Validators.required, Validators.email]),
            phone:new FormControl('', Validators.required),
            address: new FormControl('', Validators.required),
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }
    

    async add(){
        
        let customer       = new Customer();
        customer.name      = this.registerForm.get("name").value;
        customer.lastname  = this.registerForm.get("lastName").value;
        customer.email     = this.registerForm.get("email").value;
        customer.phone     = this.registerForm.get("phone").value;
        customer.address   = this.registerForm.get("address").value;
        customer.customer_id_serve = -1;
        customer.status_sincronizacion = 0;

        console.log(customer);

        try{
            await this.customerS.save(customer);
            const toast = await this.toast.create({
                header: 'Success',
                message: 'Cliente almacenado correctamente.',
                color: 'success',
                position: 'bottom',
                duration: 3000
            });
            toast.present();
        }catch(err){
            const toast = await this.toast.create({
                header: 'Erro',
                message: 'Ha ocurrido un error al agregar cliente: ' + err.message,
                color: 'danger',
                position: 'bottom',
                duration: 3000
            });
            toast.present();
        }finally{
            this.route.navigate(['/dashboard/customers']);
        }
    }

}
