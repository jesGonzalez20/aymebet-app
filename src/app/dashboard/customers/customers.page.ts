import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { LoadingController } from '@ionic/angular';
import { CustomerService } from 'src/app/services/offline/customer.service';
import { ToastController } from '@ionic/angular';
import { Customer } from 'src/app/services/class/Customer';

@Component({
    selector: 'app-customers',
    templateUrl: './customers.page.html',
    styleUrls: ['./customers.page.scss'],
})
export class CustomersPage implements OnInit {

    public skeleton: boolean = true;

    public searchForm: FormGroup;
    public customers: any = [];
    constructor(
        private loadingController: LoadingController,
        private formBuilder: FormBuilder,
        private customerS: CustomerService,
        private toast: ToastController,

    ) { }

    ngOnInit() {
        //CONSTRUIR EL FORMULARIO 
        this.searchForm = this.formBuilder.group({
            like: new FormControl('', [Validators.required]),
        });
        this.loadCustomers();
    }
    async loadCustomers() {
        try {
            this.customers = await this.customerS.getAll();
        } catch (err) {
            const toast = await this.toast.create({
                header: 'Erro',
                message: 'Ha ocurrido un error al obtener los clientes: ' + err.message,
                color: 'danger',
                position: 'bottom',
                duration: 3000
            });
            toast.present();
        } finally {
            this.skeleton = false;
        }

    }
    async onSearch() {
        this.skeleton = true;
        const loading = await this.loadingController.create({
            message: 'Espera por favor...',
            duration: 500
        });
        try {
            await loading.present();
            const like = this.searchForm.get("like").value;
            this.customers = await this.customerS.filter(like);
        } catch (err) {
            alert("Ha ocurrido un error offline " + err.message);
        } finally {
            this.skeleton = false;
            await loading.onDidDismiss();
        }
    }
    async addCustomers() {
        let customers: any = [];
        try {
            customers = await this.customerS.getCustomers('1256766');
            console.log(customers.length);

            for (var i = 0; i < customers.length; i++) {
                let customer = customers[i];

                let customerS = new Customer();
                customerS.customer_id_serve = customer.id;
                customerS.name = customer.nombre;
                customerS.email = customer.email;
                customerS.lastname = 'sin-ape';
                customerS.phone = customer.telefono;
                customerS.address = customer.direccion;
                customerS.status_sincronizacion = 1;
                await this.customerS.save(customerS);
            }
        } catch (error) {
            alert("un error ha ocurrido al registrar los clientes del servidor: "+error.message);
        } finally {
            const toast = await this.toast.create({
                header: 'Success',
                message: 'Clientes almacenados correctamente.',
                color: 'success',
                position: 'bottom',
                duration: 3000
            });
            toast.present();
            this.loadCustomers();
        }
    }
}
