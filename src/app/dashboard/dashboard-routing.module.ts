import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardPage } from './dashboard.page';

const routes: Routes = [
  {
    path: '',
    component: DashboardPage
  },
  {
    path: 'products',
    loadChildren: () => import('./products/products.module').then( m => m.ProductsPageModule)
  },
  {
    path: 'product/:id',
    loadChildren: () => import('./product-show/product-show.module').then( m => m.ProductShowPageModule)
  },
  {
    path: 'sale-current',
    loadChildren: () => import('./sale-current/sale-current.module').then( m => m.SaleCurrentPageModule)
  },
  {
    path: 'sales',
    loadChildren: () => import('./sales/sales.module').then( m => m.SalesPageModule)
  },
  {
    path: 'sale/:id',
    loadChildren: () => import('./sale-show/sale-show.module').then( m => m.SaleShowPageModule)
  },
  {
    path: 'customers',
    loadChildren: () => import('./customers/customers.module').then( m => m.CustomersPageModule)
  },
  {
    path: 'customer-menu/:id',
    loadChildren: () => import('./customer-menu/customer-menu.module').then( m => m.CustomerMenuPageModule)
  },
  {
    path: 'customer-sales/:id',
    loadChildren: () => import('./customer-sales/customer-sales.module').then( m => m.CustomerSalesPageModule)
  },
  {
    path: 'customer-credits/:id',
    loadChildren: () => import('./customer-credits/customer-credits.module').then( m => m.CustomerCreditsPageModule)
  },
  {
    path: 'credit-show/:id',
    loadChildren: () => import('./credit-show/credit-show.module').then( m => m.CreditShowPageModule)
  },
  {
    path: 'customer-add',
    loadChildren: () => import('./customer-add/customer-add.module').then( m => m.CustomerAddPageModule)
  },
  {
    path: 'sinc',
    loadChildren: () => import('./sinc/sinc.module').then( m => m.SincPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardPageRoutingModule {}
