import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CustomerCreditsPageRoutingModule } from './customer-credits-routing.module';

import { CustomerCreditsPage } from './customer-credits.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomerCreditsPageRoutingModule
  ],
  declarations: [CustomerCreditsPage]
})
export class CustomerCreditsPageModule {}
