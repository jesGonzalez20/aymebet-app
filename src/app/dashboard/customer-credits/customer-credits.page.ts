import { Component, OnInit } from '@angular/core';
import { Customer } from 'src/app/services/class/Customer';
import { ActivatedRoute } from '@angular/router';
import { CustomerService } from 'src/app/services/offline/customer.service';
import { SaleService } from 'src/app/services/offline/sale.service';
@Component({
    selector: 'app-customer-credits',
    templateUrl: './customer-credits.page.html',
    styleUrls: ['./customer-credits.page.scss'],
})
export class CustomerCreditsPage implements OnInit {

    customer: Customer = { id: 0, customer_id_serve: 0, name: '', lastname: '', email: '', phone: '', address: '' };
    idParams: number;
    credits: any[] = [];
    totalAdeuda: number = 0.0;

    constructor(
        private params: ActivatedRoute,
        private customerS: CustomerService,
        private saleS: SaleService
    ) { }

    ngOnInit() {
        this.idParams = parseInt(this.params.snapshot.params.id);
        this.loadCustomer();
        this.loadCredits();
        this.loadTotalCreditAdeuda();
    }
    async loadTotalCreditAdeuda() {
        try {
            let idCustomer = this.idParams;
            this.totalAdeuda = await this.saleS.getTotalAdeudaSaleByCustomer(idCustomer);
        } catch (error) {
            alert("Ha ocurrido un error al obtener las ventas: "+error.message);
      
        }
    }
    async loadCredits() {
        try {
            let idCustomer = this.idParams;
            this.credits = await this.saleS.getSalesToCreditBycustmer(idCustomer);
        } catch (error) {
            alert("Ha ocurrido un error al obtener las ventas: " + error.message);
        }
    }
    async loadCustomer() {
        let idCustomer = this.idParams;
        try {
            this.customer = await this.customerS.getById(idCustomer);
        } catch (err) {
            alert("Ha ocurrido un erro al traer el cliente: " + err.message);
        }
    }
    formaterAmount(amount) {
        const options2 = { style: 'currency', currency: 'USD' };
        const numberFormat2 = new Intl.NumberFormat('en-US', options2);
        return numberFormat2.format(amount);
    }


}
