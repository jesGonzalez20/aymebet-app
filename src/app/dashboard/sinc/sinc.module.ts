import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SincPageRoutingModule } from './sinc-routing.module';

import { SincPage } from './sinc.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SincPageRoutingModule
  ],
  declarations: [SincPage]
})
export class SincPageModule {}
