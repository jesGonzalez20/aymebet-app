import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SincPage } from './sinc.page';

const routes: Routes = [
  {
    path: '',
    component: SincPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SincPageRoutingModule {}
