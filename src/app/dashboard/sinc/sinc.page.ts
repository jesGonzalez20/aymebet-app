import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/services/offline/customer.service';
import { Customer } from 'src/app/services/class/Customer';
import { Product } from 'src/app/services/class/Product';
import { ProductService } from 'src/app/services/offline/product.service';

@Component({
    selector: 'app-sinc',
    templateUrl: './sinc.page.html',
    styleUrls: ['./sinc.page.scss'],
})
export class SincPage implements OnInit {

    constructor(
        private customerS: CustomerService,
        private productS: ProductService
    ) { }

    ngOnInit() {
    }


    async sincCustomers() {
        let customers: Customer[] = [];
        try {
            customers = await this.customerS.getRegisteresOffline();
        } catch (err) {
            alert("Error al traer los clientes registrados offline " + err.message);
        } finally {
            if (customers.length == 0) {
                alert("No hay clientes que sincronizar");
            } else {
                alert("Los clientes han sido sincronizados correctamente");
            }
        }

        for (var i = 0; i < customers.length; i++) {
            let customer = customers[i];
            try {
                let id_server = await this.customerS.sendCustomerToServer(customer, '1256766');
                await this.customerS.updateServerCustomer(customer.id, id_server);
            } catch (err) {
                alert("Error al actualizar el id customer server " + err.message);
            } finally {
                alert("El cliente ha sido actualizados correctamente y enviado al servidor correctamente");
            }
        }
    }

    async sincProducts() {
        let products: Product[] = [];

        try {
            products = await this.productS.getAll();
            for (let i = 0; i < products.length; i++) {
                let product = products[i];
                alert(product.product_id_serve);
                alert(product.stock);
                try {
                    await this.productS.updateStockOnServer(product.product_id_serve,product.stock,'1256766');
                } catch (error) {
                    alert("Error al sincronizar el producto: "+error.message);
                }   
            }
        } catch (error) {
            alert("Error al sincronizar los productos: "+error.message);
        }finally{
            alert("Los productos han sido sincronizados correctamente");
        }
    }

}
