import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SincPage } from './sinc.page';

describe('SincPage', () => {
  let component: SincPage;
  let fixture: ComponentFixture<SincPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SincPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SincPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
