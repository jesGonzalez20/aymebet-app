import { Component, OnInit } from '@angular/core';
import { SaleService } from '../services/offline/sale.service';
import { Sale } from '../services/class/Sale';
import { Platform } from '@ionic/angular';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.page.html',
    styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

    items:any = [
        {route:'/dashboard/products',image:'https://image.flaticon.com/icons/png/512/102/102348.png',title:'Productos'},
        {route:'/dashboard/customers',image:'https://image.flaticon.com/icons/png/512/115/115801.png',title:'Clientes'},
        {route:'/dashboard/sales',image:'https://image.flaticon.com/icons/png/512/86/86042.png',title:'Ventas'},
        {route:'/dashboard/sale-current',image:'https://image.flaticon.com/icons/png/512/83/83203.png',title:'Agregar venta'},
        {route:'/dashboard/sinc',image:'https://image.flaticon.com/icons/png/512/87/87160.png',title:'Sincronizar'},
    ];
    constructor(private saleS:SaleService,private platform: Platform,) { }

    ngOnInit() {}
    
    

}
