import { Component, OnInit } from '@angular/core';
import { Sale } from 'src/app/services/class/Sale';
import { SaleService } from 'src/app/services/offline/sale.service';

@Component({
    selector: 'app-sales',
    templateUrl: './sales.page.html',
    styleUrls: ['./sales.page.scss'],
})
export class SalesPage implements OnInit {

    sales:any[] = [];
    total:number = 0.0;

    constructor(private saleS:SaleService) { }

    ngOnInit() {
        this.loadTotalSale();
        this.loadSales();
    }
    async loadTotalSale() {
        try {
            this.sales = await this.saleS.getSales();
        } catch (error) {
            alert("Ha ocurrido un error al obtener las ventas: "+error.message);
        }
    }
    async loadSales() {
        try {
            this.total = await this.saleS.getTotalSales();
        } catch (error) {
            alert("Ha ocurrido un error al obtener las ventas: "+error.message);
      
        }
    }
    formaterAmount(amount){
        const options2 = { style: 'currency', currency: 'USD' };
        const numberFormat2 = new Intl.NumberFormat('en-US', options2);
        return numberFormat2.format(amount);
    }

}
