import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Product } from 'src/app/services/class/Product';
import { ProductService } from 'src/app/services/offline/product.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { SaleService } from 'src/app/services/offline/sale.service';
import { ShoppingbasketService } from 'src/app/services/offline/shoppingbasket.service';
import { ShoppingBasket } from 'src/app/services/class/ShoppingBasket';


@Component({
    selector: 'app-product-show',
    templateUrl: './product-show.page.html',
    styleUrls: ['./product-show.page.scss'],
})
export class ProductShowPage implements OnInit {

    public paramsID: string;
    public product: Product = { id: 0, name_product: '', product_id_serve: 0, brand: '', price1: 0, price2: 0, price3: 0, stock: 0 };

    registerForm: FormGroup;
    submitted = false;

    constructor(
        private router: ActivatedRoute,
        private productS: ProductService,
        private formBuilder: FormBuilder,
        private saleS: SaleService,
        private shoppingBS: ShoppingbasketService,
        private route:Router
    ) { }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            amount: new FormControl('', Validators.required),
            price: new FormControl('', Validators.required),
        });
        this.paramsID = this.router.snapshot.params.id;
        this.loadProduct();
    }
    async loadProduct() {
        let id = parseInt(this.paramsID);
        try {
            this.product = await this.productS.getById(id);
        } catch (err) {
            alert("Error al cargar el producto: " + err.message);
        }
    }
    async add() {
        await this.initialSale();
        
        let productId = this.product.id;
        let saleId    = localStorage.getItem('saleID');
        let amount    = this.registerForm.get('amount').value
        let price     = this.registerForm.get('price').value;


        let row = await this.shoppingBS.isOnShoppingBasket(productId,saleId);

        let shoppingbasket = new ShoppingBasket();
        shoppingbasket.price =  price;
        shoppingbasket.product_id = this.product.id;
        shoppingbasket.amount = amount;
        shoppingbasket.sale_id = parseInt(saleId);

        try {
            if(amount < this.product.stock)
                if(row.length == 0){
                    await this.shoppingBS.addOnShoppingBasket(shoppingbasket);
                    alert("Se ha guardado en la venta actual el producto");
                }else{
                    let shoppingBasketId = row.item(0).id;
                    await this.shoppingBS.updateShoppingBasket(shoppingbasket,shoppingBasketId);
                    alert("Se actualizo el producto en la venta");
                }
            else{
                alert("No hay suficiente stock");
            }
        } catch (err) {
            alert("Ha ocurrido un error al ingresar el producto: " + err.message);
        }finally{
            this.route.navigate(['/dashboard/sale-current']);
        }
    }
    async initialSale() {
        let saleCurrent = localStorage.getItem('saleID');
        if (saleCurrent == null) {
            try {
                let result = await this.saleS.initial();
                localStorage.setItem('saleID', result.insertId);
            } catch (err) {
                alert("Ha ocurrido un error al crear la venta" + err.message);
            }
        }
    }
    formaterAmount(amount){
        const options2 = { style: 'currency', currency: 'USD' };
        const numberFormat2 = new Intl.NumberFormat('en-US', options2);
        return numberFormat2.format(amount);
    }

}
