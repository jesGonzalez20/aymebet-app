import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductShowPageRoutingModule } from './product-show-routing.module';

import { ProductShowPage } from './product-show.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ProductShowPageRoutingModule
  ],
  declarations: [ProductShowPage]
})
export class ProductShowPageModule {}
