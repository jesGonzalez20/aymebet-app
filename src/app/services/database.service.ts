import { Injectable } from '@angular/core';
import { SQLiteObject, SQLite } from '@ionic-native/sqlite/ngx';

import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { ToastController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class DatabaseService {

    db: SQLiteObject;
    databaseName: string = "offlineDB.db";


    constructor(private sqlitePorter: SQLitePorter, private sqlite: SQLite, private http: HttpClient, private toast: ToastController) {

    }
    async openDatabase() {
        try {
            this.db = await this.sqlite.create({ name: this.databaseName, location: 'default' });
            await this.createTablesDatabase();
            const toast = await this.toast.create({
                header: 'Success',
                message: 'Base de datos y tablas creadas correctamente',
                color: 'success',
                position: 'bottom',
                duration: 3000
            });
            toast.present();
        } catch (err) {
            console.log("Ha ocurrido un error al procesar la creacion de la base de datos", err);
            alert("Ha ocurrido un error al procesar la creacion de la base de datos" + err.message);
        }
    }
    async createTablesDatabase() {
        let sql = await this.http.get('assets/seed.sql', { responseType: 'text' });
        return sql.subscribe(sql => {
            alert("Las tablas fueron creadas correctamente");
            return this.sqlitePorter.importSqlToDb(this.db, sql).then(res => { return res });
        },(err)=>{alert("hubo un error al crear las tablas: "+err.message)});
    }
    executeSQL(sql: string, params?: any[]) {
        return this.db.executeSql(sql, params);
    }
}
