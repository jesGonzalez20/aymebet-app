import { Injectable } from '@angular/core';
import { DatabaseService } from '../database.service';
import { Customer } from '../class/Customer';
import { HttpClient } from '@angular/common/http';
import { API } from 'src/app/config';

@Injectable({
    providedIn: 'root'
})
export class CustomerService {
    

    constructor(private db: DatabaseService,private http:HttpClient) { }

    async getAll() {
        let sql = 'SELECT * FROM customers order by id desc';
        const results = await this.db.executeSQL(sql);
        const customers = this.fillCustomer(results.rows);
        return customers;
    }

    async getRegisteresOffline() {
        let sql = 'SELECT * FROM customers where customer_id_server = -1  AND status_sincronizacion = 0';
        const results = await this.db.executeSQL(sql);
        const customers = this.fillCustomer(results.rows);
        return customers;
    }

    async save(customer:Customer) {
        let sql = 'INSERT INTO customers(customer_id_server,name_customer,lastname,email,phone,address,status_sincronizacion) values(?,?,?,?,?,?,?)';
        let params = [customer.customer_id_serve,customer.name,customer.lastname,customer.email, customer.phone,customer.address,customer.status_sincronizacion];
        return await this.db.executeSQL(sql,params);
    }
    async filter(like:string){
        let sql = 'SELECT * FROM customers where name_customer like ?';
        let params = [`%${like}%`];
        let results = await this.db.executeSQL(sql,params);
        const customers = this.fillCustomer(results.rows);
        return customers;
    }

    async getById(id:number){
        let sql = 'SELECT * FROM customers where id = ?';
        let params = [id];
        let result = await this.db.executeSQL(sql,params);

        const rows = result.rows;
        const customer = new Customer();
        if (rows.length > 0) {
            let item = rows.item(0);
            customer.id = item.id;
            customer.name = item.name_customer;
            customer.lastname = item.lastname;
        }
        return customer;
    }

    fillCustomer(rows:any) {
        let customers:Customer[] = [];
        for (let i = 0; i < rows.length; i++) {
            let item = rows.item(i);
            let customer = new Customer();
            customer.id = item.id;
            customer.customer_id_serve = item.customer_id_server;
            customer.name = item.name_customer;
            customer.lastname = item.lastname;
            customer.email = item.email;
            customer.phone = item.phone;
            customer.address = item.address;
            customers.push(customer);
        }
        return customers;
    }

    async getCustomers(codeUser){
        return await this.http.get(API.url_prod+'clientes-api?code_user='+codeUser).toPromise();
    }

    async sendCustomerToServer(customer:Customer,codeUser){
        return await this.http.post(API.url_prod+'api/clientes-api',{
            name_customer:customer.name,
            email:customer.email,
            phone:customer.phone,
            address:customer.address,
            code_user:codeUser
        }).toPromise();
    }

    async updateServerCustomer(idOffline,idServer){
        let sql = 'UPDATE customers set customer_id_server = ?, status_sincronizacion = 1 where id = ?';
        let params = [idServer,idOffline];
        return await this.db.executeSQL(sql,params);
    }
}
