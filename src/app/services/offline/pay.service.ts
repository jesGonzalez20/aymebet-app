import { Injectable } from '@angular/core';
import { DatabaseService } from '../database.service';
import { Pay } from '../class/Pay';

@Injectable({
    providedIn: 'root'
})
export class PayService {


    constructor(
        private db:DatabaseService
    ) { }

    async getPays(saleID:number) {
        let sql = 'SELECT * FROM pays where sale_id = ? order by date desc';
        let params = [saleID];
        
        let results = await this.db.executeSQL(sql,params);

        let rows = results.rows;
        let pays:Pay[] = [];
        for(let i = 0; i < rows.length; i++){
            let item    = rows.item(i);
            let pay     = new Pay();
            pay.id      = item.id;
            pay.pay     = item.pay;
            pay.sale_id = item.sale_id;
            pay.date    = item.date;
            pays.push(pay);
        }
        return pays;
    }

    async getTotalPays(saleID){
        let sql = 'SELECT sum(pay) as abono FROM pays where sale_id = ?';
        let params = [saleID];
        
        let results = await this.db.executeSQL(sql,params);

        let rows = results.rows;
        let abonoTotal = 0.0;
        if(rows.length > 0){
            abonoTotal = rows.item(0).abono;
        }
        return abonoTotal;
    }

    async addPay(pay:number,saleID:number){
        let sql = 'INSERT INTO pays(pay,sale_id,date) values(?,?,?)';
        let date = new Date();
        let params = [pay,saleID,date];
        await this.db.executeSQL(sql,params);
    }
  



}
