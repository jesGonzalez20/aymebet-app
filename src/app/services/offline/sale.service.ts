import { Injectable } from '@angular/core';
import { DatabaseService } from '../database.service';
import { Sale } from '../class/Sale';

@Injectable({
    providedIn: 'root'
})
export class SaleService {


    constructor(private db: DatabaseService) { }

    async initial() {
        let sql = 'INSERT INTO sales(total,type_sale,date,customer_id,status) values(?,?,?,?,?)';
        let date = new Date();
        let params = [0.0, 0, date, 0, 0];
        return await this.db.executeSQL(sql, params);
    }
    async endSale(sale: Sale) {
        let sql = 'UPDATE sales set total = ?, type_sale = ? , customer_id = ? , status = ? where id = ?';
        let params = [sale.total, sale.type_sale, sale.customer_id, sale.status, sale.id];
        return await this.db.executeSQL(sql, params);
    }

    async getSales() {
        let sql = 'SELECT * FROM sales where status != 0 AND type_sale = 0 order by id desc';
        let results = await this.db.executeSQL(sql);
        let rows = results.rows;
        let sales: any[] = [];
        for (let i = 0; i < rows.length; i++) {
            let item = rows.item(i);
            sales.push(item);
        }
        return sales;
    }
    async getById(id: number) {
        let sql = 'SELECT * FROM sales where id = ?';
        let params = [id];
        let results = await this.db.executeSQL(sql, params);
        let rows = results.rows;
        const sale = new Sale();
        if (rows.length > 0) {
            let item = rows.item(0);
            sale.id = item.id;
            sale.customer_id = item.customer_id;
            sale.date = item.date;
            sale.total = item.total;
            sale.status = item.status;
            sale.type_sale = item.type_sale;
        }
        return sale;
    }
    async getTotalSales() {
        let sql = 'SELECT sum(total) AS total FROM sales where status = 1';
        let result = await this.db.executeSQL(sql);
        let total = 0.0;
        if (result.rows.length > 0) {
            let item = result.rows.item(0);
            total = item.total;
        }
        return total;
    }
    async getTotalSaleByCustomer(idCustomer:number) {
        let sql = 'SELECT sum(total) AS total FROM sales where status = 1 AND customer_id = ? ';
        let params = [idCustomer];
        let result = await this.db.executeSQL(sql,params);
        let total = 0.0;
        if (result.rows.length > 0) {
            let item = result.rows.item(0);
            total = item.total;
        }
        return total;
    }
    async getSalesByIdCustomer(idCustomer: number) {
        let sql = 'SELECT * FROM sales where status != 0 AND customer_id = ? AND type_sale = 0 order by id desc';
        let params = [idCustomer];
        let results = await this.db.executeSQL(sql, params);
        let rows = results.rows;
        let sales: any[] = [];
        for (let i = 0; i < rows.length; i++) {
            let item = rows.item(i);
            sales.push(item);
        }
        return sales;
    }
    async cancelSale(saleID:number) {
        let sql = 'UPDATE sales set status = 2 where id = ?';
        let params = [saleID];
        return await this.db.executeSQL(sql,params);
    }

    async getSalesToCreditBycustmer(idCustomer:number) {
        let sql = 'SELECT * FROM sales where type_sale = 1 AND customer_id = ?';
        let params = [idCustomer];
        let results = await this.db.executeSQL(sql, params);
        let rows = results.rows;
        let sales: any[] = [];
        for (let i = 0; i < rows.length; i++) {
            let item = rows.item(i);
            sales.push(item);
        }
        return sales;
    }
    async getTotalAdeudaSaleByCustomer(idCustomer:number) {
        let sql = 'SELECT sum(total) AS total FROM sales where type_sale = 1 AND status = 3 AND customer_id = ? ';
        let params = [idCustomer];
        let result = await this.db.executeSQL(sql,params);
        let total = 0.0;
        if (result.rows.length > 0) {
            let item = result.rows.item(0);
            total = item.total;
        }
        return total;
    }

    async creditPayed(saleID:number) {
        let sql = 'UPDATE sales set status = 4 where id = ?';
        let params = [saleID];
        return await this.db.executeSQL(sql,params);
    }
    
}
