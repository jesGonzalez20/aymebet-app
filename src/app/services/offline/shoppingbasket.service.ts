import { Injectable } from '@angular/core';
import { DatabaseService } from '../database.service';
import { ShoppingBasket } from '../class/ShoppingBasket';

@Injectable({
    providedIn: 'root'
})
export class ShoppingbasketService {

    constructor(private db: DatabaseService) { }

    async addOnShoppingBasket(product: ShoppingBasket) {
        let sql = 'INSERT INTO shopping_basket(product_id,sale_id,amount,price) values(?,?,?,?)';
        let params = [product.product_id, product.sale_id, product.amount, product.price];
        return await this.db.executeSQL(sql, params);
    }
    async isOnShoppingBasket(product_id,sale_id){
        let sql = 'SELECT * FROM shopping_basket WHERE product_id = ? and sale_id = ?';
        let params = [product_id,sale_id];
        let result = await this.db.executeSQL(sql,params);
        let rows   = result.rows;
        return rows;
    }

    async updateShoppingBasket(product:ShoppingBasket,shoppingBasketId){
        let sql = `UPDATE shopping_basket set product_id = ?, sale_id = ?, amount = ? , price = ? where id = ${shoppingBasketId}`;
        let params =  [product.product_id, product.sale_id, product.amount, product.price];
        return await this.db.executeSQL(sql, params);
    }

    async getTotalSale(saleID:number){
        let sql = 'select sum(price * amount) as total from shopping_basket  where sale_id = ?';
        let params = [saleID];
        let result = await this.db.executeSQL(sql,params);
        let total = 0.0;
        if(result.rows.length > 0){
            let item = result.rows.item(0);
            total = item.total;
        }
        return total;
    }

    async getProducts(saleID:number) {
        let sql = 'select products.id as product_id, (shopping_basket.price * shopping_basket.amount) AS subtotal, products.name_product AS product, shopping_basket.price , shopping_basket.amount from shopping_basket JOIN products ON shopping_basket.product_id = products.id where shopping_basket.sale_id = ?';
        let params = [saleID];
        let results = await this.db.executeSQL(sql,params);
        let rows = results.rows;

        let products:any[] = [];

        for(let i = 0; i < rows.length; i++){
            let item = rows.item(i);
            products.push(item);
        }   
        return products;
    }
}
