import { TestBed } from '@angular/core/testing';

import { ShoppingbasketService } from './shoppingbasket.service';

describe('ShoppingbasketService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ShoppingbasketService = TestBed.get(ShoppingbasketService);
    expect(service).toBeTruthy();
  });
});
