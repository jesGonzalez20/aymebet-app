import { Injectable } from '@angular/core';
import { DatabaseService } from '../database.service';
import { Product } from '../class/Product';
import { HttpClient } from '@angular/common/http';
import { API } from 'src/app/config';
@Injectable({
    providedIn: 'root'
})
export class ProductService {

    constructor(private db: DatabaseService, private http:HttpClient) { }

    async getAll() {
        const sql = 'SELECT * FROM products';
        const result = await this.db.executeSQL(sql);
        const products = this.fillProducts(result.rows);
        return products;
    }
    async filter(like: string) {
        let sql = 'SELECT * FROM products where name_product like ?';
        let params = [`%${like}%`];
        const result = await this.db.executeSQL(sql, params);
        const products = this.fillProducts(result.rows);
        return products;
    }
    async getById(id:number){
        let sql = 'SELECT * FROM products where id = ?';
        let params = [id];
        let result = await this.db.executeSQL(sql,params);
        const rows = result.rows;
        const product = new Product();
        if (rows.length > 0) {
            let item = rows.item(0);
            product.id = item.id;
            product.name_product = item.name_product;
            product.brand = item.brand;
            product.price1 = item.price1;
            product.price2 = item.price2;
            product.price3 = item.price3;
            product.stock = item.stock;
        }
        return product;
    }
    fillProducts(rows: any) {
        const products: Product[] = [];
        for (let i = 0; i < rows.length; i++) {
            const item = rows.item(i);
            const product = new Product();
            product.id = item.id;
            product.name_product = item.name_product;
            product.brand = item.brand;
            product.price1 = item.price1;
            product.price2 = item.price2;
            product.price3 = item.price3;
            product.stock = item.stock;
            product.product_id_serve = item.product_id_server;
            products.push(product);
        }
        return products;
    }
    async save(product: Product) {
        let sql = 'INSERT INTO products(product_id_server,name_product,brand,price1,price2,price3,stock) values(?,?,?,?,?,?,?)';
        let params = [product.product_id_serve, product.name_product, product.brand, product.price1, product.price2, product.price3, product.stock];
        return await this.db.executeSQL(sql, params);
    }

    async update(idProduct,amount){
        let sql = "UPDATE products set stock = ? where id = ?";
        let params = [amount,idProduct];
        return await this.db.executeSQL(sql, params);
    }

    async getProductsServer(codeUser)
    {
        return await this.http.get(API.url_prod+'productos?code_user='+codeUser).toPromise();
    }
    
    async updateStockOnServer(idProductServer,amountCurrent,codeUser){
        return await this.http.post(API.url_prod+'api/productos-api',{
            product_id:idProductServer,
            amount:amountCurrent,
            code_user:codeUser
        }).toPromise()
    }


}
