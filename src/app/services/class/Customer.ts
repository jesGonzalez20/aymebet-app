export class Customer{
    id:number;
    customer_id_serve:number;
    name:string;
    lastname:string;
    email:string;
    phone:string;
    address:string;
    status_sincronizacion?:number;
}