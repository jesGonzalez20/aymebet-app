export class ShoppingBasket{
    id?:number;
    product_id:number;
    amount:number;
    price:number;
    sale_id:number;
    subtotal?:number;
}