import { Component, OnInit } from '@angular/core';
import { IClient } from 'src/app/interfaces/Client';
import { DatabaseService } from 'src/app/services/database.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-clients-add',
    templateUrl: './clients-add.page.html',
    styleUrls: ['./clients-add.page.scss'],
})
export class ClientsAddPage implements OnInit {

    client:IClient = {nombre:'',apellido:'',email:'',telefono:'',direccion:''};

    constructor(private db:DatabaseService,private router:Router) { }

    ngOnInit() {
       
    }

    addClient()
    {
    
    }

}
