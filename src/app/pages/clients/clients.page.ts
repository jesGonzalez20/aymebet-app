import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/services/database.service';
import { IClient } from 'src/app/interfaces/Client';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-clients',
    templateUrl: './clients.page.html',
    styleUrls: ['./clients.page.scss'],
})
export class ClientsPage implements OnInit {

    constructor(private db: DatabaseService) { }

    ngOnInit() {
        
    }

}
