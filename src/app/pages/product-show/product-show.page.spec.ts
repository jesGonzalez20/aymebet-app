import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProductShowPage } from './product-show.page';

describe('ProductShowPage', () => {
  let component: ProductShowPage;
  let fixture: ComponentFixture<ProductShowPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductShowPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductShowPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
