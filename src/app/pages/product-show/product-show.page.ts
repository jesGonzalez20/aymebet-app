import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DatabaseService } from 'src/app/services/database.service';
import { IProduct } from 'src/app/interfaces/Product';

@Component({
    selector: 'app-product-show',
    templateUrl: './product-show.page.html',
    styleUrls: ['./product-show.page.scss'],
})
export class ProductShowPage implements OnInit {
    id: string;
    product:IProduct = {nombre:'',marca:'',p1:0.00,p2:0.00,p3:0.0,existencia:0};
    options:any = {};
    constructor(private router: ActivatedRoute, private db:DatabaseService) { }

    ngOnInit() {
        /*this.id = this.router.snapshot.params.id;
        this.db.getDatabaseState().subscribe(rdy=>{
            this.db.getProduct(this.id).then(product=>{
                this.product = product;
            }).catch(err=>{
                alert("No fue posible cargar el producto");
            });
        },(err)=>{
            alert("No cargado...");
        });*/
    }
    addToSale()
    {
        /*this.db.getDatabaseState().subscribe(rdy=>{
            if(rdy){
                this.db.addSale();
            }
        })
        this.db.onProdVen(1,this.product.id).then((res)=>{
            if(res.rows.length == 0){
                this.db.addProdVen(this.options,this.product.id,1);
            }else{
                this.db.updateProdVen(this.options,this.product.id,res.rows.item(0),1);
            }
        }).catch(err => alert("error ha ocurrido"));*/
    }
}
