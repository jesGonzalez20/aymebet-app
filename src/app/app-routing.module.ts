import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'clients',
    loadChildren: () => import('./pages/clients/clients.module').then( m => m.ClientsPageModule)
  },
  {
    path: 'clients-add',
    loadChildren: () => import('./pages/clients-add/clients-add.module').then( m => m.ClientsAddPageModule)
  },
  {
    path: 'add-sale',
    loadChildren: () => import('./pages/add-sale/add-sale.module').then( m => m.AddSalePageModule)
  },
  {
    path: 'product-show/:id',
    loadChildren: () => import('./pages/product-show/product-show.module').then( m => m.ProductShowPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then( m => m.DashboardPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
