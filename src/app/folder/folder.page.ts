import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DatabaseService } from '../services/database.service';
import { IProduct } from '../interfaces/Product';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-folder',
    templateUrl: './folder.page.html',
    styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
    public folder: string;
    products: Observable<IProduct[]>;
    constructor(private activatedRoute: ActivatedRoute, private db: DatabaseService) { }

    ngOnInit() {
       
    }

    addProduct()
    {
        
    }

}
