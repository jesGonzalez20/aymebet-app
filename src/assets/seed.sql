/*create table if not exists users(id INTEGER auto_increment not null,email varchar(100) not null,password varchar(200) not null,id_user integer not null,name_user varchar(50),primary key(id));*/
create table if not exists  products(id INTEGER PRIMARY KEY AUTOINCREMENT not null,product_id_server INTEGER not null,name_product varchar(30) not null,brand varchar(20) not null,price1 double not null,price2 double not null,price3 double not null,stock integer not null,status_sincronizacion INTEGER DEFAULT 0);

create table if not exists  sales(id INTEGER PRIMARY KEY AUTOINCREMENT not null,total DOUBLE not null,customer_id INTEGER not null,date DATE, type_sale INTEGER, status INTEGER,status_sincronizacion INTEGER DEFAULT 0);

create table if not exists customers(id INTEGER PRIMARY KEY AUTOINCREMENT not null,customer_id_server INTEGER null,name_customer varchar(30),lastname varchar(30),email varchar(30) null,phone varchar(15),address text,status_sincronizacion INTEGER DEFAULT 0);

create table if not exists  shopping_basket(id INTEGER PRIMARY KEY AUTOINCREMENT not null,sale_id INTEGER not null,product_id INTEGER not null,amount INTEGER not null,price DOUBLE not null,status_sincronizacion INTEGER DEFAULT 0);


create table if not exists  pays(id INTEGER PRIMARY KEY AUTOINCREMENT not null,sale_id INTEGER,pay DOUBLE,date DATE,status_sincronizacion INTEGER DEFAULT 0);



/*

create table if not exists  pays(id INTEGER PRIMARY KEY AUTOINCREMENT not null,sale_id INTEGER,pay DOUBLE,date DATE);

*/